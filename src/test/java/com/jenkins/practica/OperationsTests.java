package com.jenkins.practica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.jenkins.utils.DogsOperations;

public class OperationsTests {

	/**
	 * Checks if the operation getRandomDogImage returns a .jpg
	 */
	@Test
	public void testRandom() {
		// Instantiate DogsOperations class
		DogsOperations dogsOperationsInstance = new DogsOperations();
		String randomImageResult;
		// Call getRandomDogImage operation and store the result on String
		randomImageResult = dogsOperationsInstance.getRandomDogImage();
		// Assert true if the result string ends with '.jpg'
		//assertFalse(randomImageResult.contains("jpg"));
	}
	
	/**
	 * Checks if the operation getBreedList returns a list of dogs (f.e. size > 0)
	 */
	@Test
	public void breedsList() {
		// Instantiate DogsOperations class
		DogsOperations dogsOpetarionsInstance = new DogsOperations();
		ArrayList<String> breedList;
		// Call getBreedList operation and store the result on ArrayList
		breedList = dogsOpetarionsInstance.getBreedList();
		// Assert true if the result ArrayList has size of more than 0
		//assertFalse(breedList.size() > 0);
	}
}
